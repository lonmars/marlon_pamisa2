var path =require('path');
var webpack =require('webpack');

module.exports = {
	entry:['./assets/js/app.js'],
	output :{
			filename : './resources/js/app.all.js'
		},
	module : {
		loaders:[{
			test:/\.js$/,
			include: path.resolve(__dirname,"assets/js"),
			loaders: 'babel-loader',
			query:{
				presets: ['es2015']
			}
		}]
	}	

}