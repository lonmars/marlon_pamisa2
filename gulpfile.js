var gulp =require('gulp');
var sass =require('gulp-sass');
var browserSync = require('browser-sync').create(); 




gulp.task('serve',['sass'],function(){
	
	/*
	
	browserSync.init({
		server:{
			baseDir: 'resources',
			//directory: true,
			index: 'html/index.html'

		}

	});

*/	
	browserSync.init({
        server: "./resources"
    });

	gulp.watch('./assets/sass/*.scss',['sass']);
	gulp.watch('./assets/sass/modules/login/*.scss',['sass']);
	gulp.watch('./assets/sass/modules/profile/*.scss',['sass']);
	gulp.watch("./resources/html/*.html").on('change',browserSync.reload);
	

//	gulp.watch("app/*.html").on('change', browserSync.reload);

});

gulp.task('sass', function(){
	return gulp.src('./assets/sass/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('resources/css'))
		//.pipe(browserSync.reload({stream:true}));
		.pipe(browserSync.stream());

});


gulp.task('default',['serve']);